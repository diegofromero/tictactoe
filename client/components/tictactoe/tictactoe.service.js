'use strict';

angular.module('tictactoeApp')
  .factory('Tictactoe', Tictactoe);

Tictactoe.$inject = ['$rootScope'];

function Tictactoe($rootScope) {
  let tictactoe = {};
  const emptyCell = '-';

  tictactoe.board = [
    [{value: emptyCell}, {value: emptyCell}, {value: emptyCell}],
    [{value: emptyCell}, {value: emptyCell}, {value: emptyCell}],
    [{value: emptyCell}, {value: emptyCell}, {value: emptyCell}]
  ];
  tictactoe.winner = false;
  tictactoe.cat = false;
  tictactoe.currentPlayer = 'X';

  tictactoe.isMatch = isMatch;
  tictactoe.move = move;
  tictactoe.checkForEndOfGame = checkForEndOfGame;
  tictactoe.isTaken = isTaken;
  return tictactoe;

  function isMatch(cell1, cell2, cell3) {
    return cell1.value === cell2.value &&
      cell1.value === cell3.value &&
      cell1.value !== emptyCell;
  }

  function checkForEndOfGame() {
    var rowMatch = _.reduce([0, 1, 2], function (memo, row) {
      return memo || isMatch(tictactoe.board[row][0], tictactoe.board[row][1], tictactoe.board[row][2]);
    }, false);

    var columnMatch = _.reduce([0, 1, 2], function (memo, col) {
      return memo || isMatch(tictactoe.board[0][col], tictactoe.board[1][col], tictactoe.board[2][col]);
    }, false);

    var diagonalMatch = isMatch(tictactoe.board[0][0], tictactoe.board[1][1], tictactoe.board[2][2]) ||
      isMatch(tictactoe.board[0][2], tictactoe.board[1][1], tictactoe.board[2][0]);

    tictactoe.winner = rowMatch || columnMatch || diagonalMatch;
    if (!tictactoe.winner) {
      tictactoe.cat = _.reduce(_.flatten(tictactoe.board), function (memo, cell) {
        return memo && cell.value !== emptyCell;
      }, true);
    }

    return tictactoe.winner || tictactoe.cat;
  }

  function move(cell) {
    cell.value = tictactoe.currentPlayer;
    if (checkForEndOfGame() === false) {
      tictactoe.currentPlayer = tictactoe.currentPlayer === 'X' ? 'O' : 'X';
    }
  }

  function isTaken(cell) {
    return cell.value !== emptyCell;
  }
}
