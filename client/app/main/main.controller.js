'use strict';

angular.module('tictactoeApp')
  .controller('MainCtrl', MainController);

MainController.$inject = ['Tictactoe', '$http'];
function MainController(Tictactoe, $http) {
  var vm = this;
  vm.awesomeThings = [];
  vm.tictactoe = Tictactoe;

  $http.get('/api/things').success(function(awesomeThings) {
    vm.awesomeThings = awesomeThings;
  });

  vm.addThing = function() {
    if(vm.newThing === '') {
      return;
    }
    $http.post('/api/things', { name: vm.newThing });
    vm.newThing = '';
  };

  vm.deleteThing = function(thing) {
    $http.delete('/api/things/' + thing._id);
  };
}
